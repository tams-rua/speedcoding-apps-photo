<!DOCTYPE html>
<html lang="en">

<head>
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../css/style.css">
</head>

<body>
    <div class="container-fluid">
        <div class="text-center mt-4">
            <div class="d-flex flex-row-reverse bd-highlight">
                <div class="p-2 btn btn-primary">CONNEXION</div>
                <div class="p-2 bd-highlight">A PROPOS</div>
                <div class="p-2 bd-highlight">CONTACT</div>
                <div class="p-2 bd-highlight">ACCUEIL</div>
            </div>
        </div>

        <?php 
            include 'db_connection.php';
            $veilleId = $_GET['veille_id'];
            echo $veilleId;
            $query = "SELECT * FROM `veilles` WHERE id=$veilleId";
            $sth = $bdd->prepare($query);
            $sth->execute();
            $veille = $sth->fetchAll(PDO::FETCH_ASSOC);
            
        ?>
        
        <div class="row text-center mt-4">
            <div class="card w-100">
                <img class="card-img-top" src="../img/<?php echo $veille[0]['image']; ?>" height="400" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title"><?php echo $veille[0]['date']; ?></h5>
                    <p class="card-text"><?php echo $veille[0]['sujet']; ?></p>
                    <p class="card-text"><?php echo $veille[0]['lien']; ?></p>
                    <p class="card-text"><?php echo $veille[0]['commentaire']; ?></p>
                    <p class="card-text"><?php echo $veille[0]['synthèse']; ?></p>
                    
                    <a href="#" class="btn btn-primary" onClick="displayVeille()">Go somewhere</a>
                </div>
            </div>
        </div>

</body>
<script src="js/scriptModal.js"></script>

</html>